@extends('layouts.header')

@section('calendercontent')

<div class="container">
	<div class="row">
	    <div class="col-sm-3 col-md-3 col-lg-3">
        @section('content')
       
    <sidebar-component></sidebar-component>  
</div>
<script type="text/javascript" src="{{ asset('js/app.js')  }}"></script>
@endsection

	<!-- Button to Open the Modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Create event</button>

<!-- The Modal for add new events  -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create Events Here ...</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <form action="{{route('cal.store')}}" method="POST" role="form">
        {{csrf_field()}}
        <center>  <legend>
            Create Event
        </legend></center>
        <div class="form-group">
            <label for="title">
                Title
            </label>
            <input class="form-control" name="title" placeholder="Title" type="text">
        </div>
        <div class="form-group">
            <label for="description">
                Description
            </label>
            <input class="form-control" name="description" placeholder="Description" type="text">
        </div>
        <div class="form-group">
            <label for="start_date">
                Start Date
            </label>
            <input class="form-control" name="start_date" placeholder="2015-05-28T09:00:00-07:00" type="text">
        </div>
        <div class="form-group">
            <label for="end_date">
                End Date
            </label>
            <input class="form-control" name="end_date" placeholder="2015-05-28T09:00:00-07:00" type="text">
        </div>
        
        <input type="button" name="abc" onclick="addattendee();" class="btn btn-default" value="add attendees">
        <input type="hidden" name="attendee_arr" id="attendee_arr">
        <br><br>
        <button class="btn btn-primary" type="submit">
            Submit
        </button>
        <br>
          </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<!-- closing the modal -->
<div class="sidenav">
  <a href=""><b><i>Edit Events by clicking on title </i></b></a>
      <?php $array = json_decode(json_encode($data),true); 
                    foreach ($array as $singleArray) {  
                      ?>
    <p id="title-txt" ><i><a data-toggle="modal" data-target="#myModal<?= $singleArray['id'] ?> " style="color:#01050a;" >  <?= $singleArray['title'] ?> </a> 
    <a href="{{ url('/deleteevent',[$singleArray['id']] )  }} ">Delete</a> </i></p>

<!-- Model for updating events --->                         
<div class="modal" id="myModal<?= $singleArray['id'] ?>">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Events</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body border">
          <form action="/updateevent" method="POST" role="form">
        {{csrf_field()}}
        <center>  <legend>
            Create Event
        </legend></center>
        <input type="hidden" name="eventId" value="<?= $singleArray['id'] ?>">
        <div class="form-group">
            <label for="title">
                Title
            </label>
            <input class="form-control" name="title" placeholder="Title" type="text" value="<?= $singleArray['title'] ?>">
        </div>
        <div class="form-group">
            <label for="description">
                Description
            </label>
            <input class="form-control" name="description" placeholder="Description" type="text" value="<?= $singleArray['description'] ?>">
        </div>
        <div class="form-group">
            <label for="start_date">
                Start Date
            </label>
            <input class="form-control" name="start_date" placeholder="Start Date" type="text" value="<?= $singleArray['start'] ?>">
        </div>
        <div class="form-group">
            <label for="end_date">
                End Date
            </label>
            <input class="form-control" name="end_date" placeholder="End Date" type="text" value="<?= $singleArray['end'] ?>">
        </div>
        
        <button class="btn btn-primary" type="submit">
            Submit
        </button>
        <br>
    </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div><!-- closing the updating evets modal --->
<?php
    }         
    ?>
    </div><!-- closing the sidebar --->

    </div> <!-- closing the 3 coloms -->

	    <div class="col-sm-9 col-md-9 col-lg-9">
        @if(Session::get('deletion_success'))
        <div class="alert alert-danger">
          <strong>Event Deleted Successfully </strong>
          <button class="btn btn-default" data-dismiss="alert">x</button>
        </div>
        @endif
        @if(Session::get('eventadded_success'))
        <div class="alert alert-success">
          <strong>Event Created Successfully </strong>
          <button class="btn btn-default" data-dismiss="alert">x</button>
        </div>
        @endif
	    	<div id="calendar"></div>
	    </div> <!-- closing colom 9 --->
	</div><!-- row closing --->
</div><!-- container close -->

<script type="text/javascript">
	$(document).ready(function() {
  // page is now ready, initialize the calendar...
  $('#calendar').fullCalendar({

   events : 'api/cal'
  
  });
  });
</script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>

@endsection
