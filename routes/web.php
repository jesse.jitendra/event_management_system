<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/', function () {
    return view('welcome');
});

Route::resource('gcalendar', 'gCalendarController');
Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'gCalendarController@oauth']);

Route::group(['middleware' => ['checkauth'] ], function() {

  Route::get('/register', function(){
    return view('signup');
 });
 
 Route::get('/login',function(){
   return view('login');
 });

});

  Route::group(['middleware' => ['restrictuser']], function() {

  Route::any('/logout','LoginController@logout');

  Route::get('/events', 'userevents@index');
  
  Route::get('/allevent', 'fetcchevent@index');
 
});

Route::get('/event',function(){
  return view('userdashboard');
});

Route::post('/userevent/{userId}','getusers@showevent');
Route::post('/thisuserevent','getusers@thisuserevent');
 
Route::post('/updateevent','gCalendarController@update');
Route::post('/signup','SignupController@store');
Route::post('/log','LoginController@index');
Route::any('/deleteevent/{eventId}', 'gCalendarController@destroy');
Route::resource('tasks','SignupController');

Route::post('/submit','VuesignupController@store');

Route::post('/loginsubmit','VueloginController@store');

Route::get('/justcheck','getusers@index');

Route::get('/userdetails','getusers@userdetails');

Route::get('/adminprofile', function(){
    return view('adminprofile');
});

Route::get('/admindetails', 'adminprofile@admindetails');

Route::post('/userprofile', 'userprofile@index');

Route::post('/editprofile','adminprofile@editprofile')->name('editprofile') ;

Route::get('/themelogin',function() {
  return view('themelogin');
});

Route::get('/adminevent','fetcchevent@events');
Route::post('/deleteuser/{userId}','getusers@deleteuser');
Route::post('/createevent','gCalendarController@store');
Route::post('/edituserprofile','userprofile@editprofile');