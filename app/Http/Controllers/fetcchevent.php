<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Illuminate\Http\Request;


class fetcchevent extends Controller
{
    //
    public function __construct()
    {
        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $this->client = $client;
    }
    //  function for fetching all events from google calender for show in sidebar 
    public function index(Request $request)
    {
    	
    	session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $calendarId = 'primary';

            $results = $service->events->listEvents($calendarId); // we can pass here optionl parameters also 
          
            $allevents = $results->getItems();
            
            $allevents = $results->getItems();

            $data=[];
            foreach ($allevents as $event) {
               $subArr = [
                 'id'=> $event->id,
                 'title'=>$event->getSummary(),
                 'start'=>$event->getStart()->getDateTime(),
                 'end'=>$event->getEnd()->getDateTime(),
                 'description'=>$event->description,
                 'attendees'=>$event->getattendees()
               ];
               array_push($data,$subArr);
            }
            // return $data;
             $dataobject = (object) $data;
             return view('managerdashboard',['data'=>$dataobject]);

        } else {
            return redirect()->route('oauthCallback');
        }
    }
    //  end function 
    // start function for fetching event for show in vue component
    public function events(Request $request)
    {
    	
    	session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $calendarId = 'primary';

            $results = $service->events->listEvents($calendarId); // we can pass here optionl parameters also 
          
            $allevents = $results->getItems();
            
            $allevents = $results->getItems();

            $data=[];
            foreach ($allevents as $event) {
               $subArr = [
                 'id'=> $event->id,
                 'title'=>$event->getSummary(),
                 'start'=>$event->getStart()->getDateTime(),
                 'end'=>$event->getEnd()->getDateTime(),
                 'description'=>$event->description,
                 'attendees'=>$event->getattendees()
               ];
               array_push($data,$subArr);
            }
             return $data;
            //  $dataobject = (object) $data;
            //  return view('managerdashboard',['data'=>$dataobject]);

        } else {
            return redirect()->route('oauthCallback');
        }
    }
}

