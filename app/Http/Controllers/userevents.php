<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\signup;
use App\events;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Facade;
use Auth;

class userevents extends Controller
{
    //function for getting  events for a individual person 
    public function index(Request $request)
    {

       
            $email = Auth::user()->email;
            $query = events::where('attendee1',$email)
                       ->orWhere('attendee2',$email)
                       ->orWhere('attendee3',$email)
                       ->orWhere('attendee4',$email)
                       ->orWhere('attendee5',$email)
                      ->get();
                    
                      $data=[];
                      foreach ($query as $event) {
                        $subArr = [
                            'id'=> "".$event['id']."",
                            'title'=>$event['title'],
                            'start'=>$event['start'],
                            'end'=>$event['end']
                        ];
                   array_push($data,$subArr);
                }
                 
                 return $data;
        

       
    }
    // end function 
}
