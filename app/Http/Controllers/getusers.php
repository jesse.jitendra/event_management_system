<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\vuelogin;
use App\events;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class getusers extends Controller
{
    //
    public function index()
    {
        $admin_id = 1;
        $query = vuelogin::where('id','!=',$admin_id)->get()->toArray();
        return $query;
    }
    public function userdetails()
    {
        $user_id = Auth::user()->id;
        $query = vuelogin::where('id',$user_id)
                         ->get()->toArray();
       return $query;                

    }
    public function deleteuser($userId)
    {
       $query = vuelogin::where('id',$userId)
                ->delete();
                return "success";
    }

    // function for get specific user event 
    public function  showevent($userId)
    {
        
        $query = vuelogin::where('id',$userId)
                    ->first();
        $detailarray = $query;
        $dtarray = $detailarray;
        $email = $dtarray['email'];
        $name = $dtarray['name'];
        $id = $dtarray['id'];
        
        $query =   events::where('attendee1',$email)
                   ->orWhere('attendee2',$email)
                   ->orWhere('attendee3',$email)
                   ->orWhere('attendee4',$email)
                   ->orWhere('attendee5',$email)
                  ->get();
                
                  $data=[];
                  foreach ($query as $event) {
                    $subArr = [
                        'id'=> "".$event['id']."",
                        'title'=>$event['title'],
                        'start'=>$event['start'],
                        'end'=>$event['end']
                    ];
               array_push($data,$subArr);
            }
            
             return view('simulateuser',['data'=>$data,'name'=>$name,'id'=>$id]); 
    }
    // end function 
    // function for getting all events for  a perticular person 
    public function thisuserevent(Request $request)
    {
      $userId =  $request->id;
      $query = vuelogin::where('id',$userId)
                    ->first();
        $detailarray = $query;
        $dtarray = $detailarray;
        $email = $dtarray['email'];
        $name = $dtarray['name'];
        $id = $dtarray['id'];
        
        $query =   events::where('attendee1',$email)
                   ->orWhere('attendee2',$email)
                   ->orWhere('attendee3',$email)
                   ->orWhere('attendee4',$email)
                   ->orWhere('attendee5',$email)
                  ->get();
                
                  $data=[];
                  foreach ($query as $event) {
                    $subArr = [
                        'id'=> "".$event['id']."",
                        'title'=>$event['title'],
                        'start'=>$event['start'],
                        'end'=>$event['end']
                    ];
               array_push($data,$subArr);
            }
           $jsondata = json_encode($data);
           return $data;

    }
    // end function 
    public function usersimulatedashboard($id)
    {
      return view('userdashboard',['id'=>$id]);
    }
    
}
