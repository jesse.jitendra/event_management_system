<?php

namespace App\Http\Controllers;

use App\vuelogin;
use Illuminate\Http\Request;
use App\Http\Requests\vueloginrequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Auth;

class VueloginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(vueloginrequest $request)
    {
        //
        $remember = false;
         if($request->input('remember') != null){
           $remember = true;
         }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $remember)) {
            // Authentication passed...
            $userId =   Auth::user()->id;
            $id = json_encode($userId);
            return request()->json(200,$id);
        }
        else{
            return  request()->json(200,'');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\vuelogin  $vuelogin
     * @return \Illuminate\Http\Response
     */
    public function show(vuelogin $vuelogin)
    {
        //
    }
    /**
     * Show the form for editing the specified resource. 
     *
     * @param  \App\vuelogin  $vuelogin
     * @return \Illuminate\Http\Response
     */
    public function edit(vuelogin $vuelogin)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vuelogin  $vuelogin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vuelogin $vuelogin)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vuelogin  $vuelogin
     * @return \Illuminate\Http\Response
     */
    public function destroy(vuelogin $vuelogin)
    {
        //
    }
}
