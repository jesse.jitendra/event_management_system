<?php

namespace App\Http\Controllers;

use App\login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // function for mak login user this function use for both manager login and user login 
    public function index(Request $request)
    {
        //
         $request->validate([
        'email' => 'required|email',
        'password'=>'required'
        ]);

        $remember = false;
         if($request->input('remember') != null){
           $remember = true;
         }
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $remember)) {
            // Authentication passed...
            $userId =   Auth::user()->id;
            if($userId  == 1)
            {
                $request->session()->flash('loginsuccess');
                return redirect('/allevent');
            }else{
                return redirect('/event');
            }
        }
        else{
            $request->session()->flash('login_unsuccess');
            return redirect('/login');
        }
    }
    // end function 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\login  $login
     * @return \Illuminate\Http\Response
     */
    public function show(login $login)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\login  $login
     * @return \Illuminate\Http\Response
     */
    public function edit(login $login)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\login  $login
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, login $login)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\login  $login
     * @return \Illuminate\Http\Response
     */
    public function destroy(login $login)
    {
        //
    }
    // function for logout 
    public function logout()
    {
       Auth::logout();
       Session::flush();
       return redirect('/');
    }
}
