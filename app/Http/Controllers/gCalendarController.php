<?php

namespace App\Http\Controllers;

use App\events;
use Carbon\Carbon;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendMail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;
use URL;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\editevent;
use App\Http\Requests\createevent;


class gCalendarController extends Controller
{
    protected $client;

    public function __construct()
    {
        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $this->client = $client;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // functiokn for returning all events from google calender to show on full calender  
    public function index()
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            // echo  '<pre>';
            // print_r($_SESSION['access_token']) ;
            // exit();
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $calendarId = 'primary';

            $results = $service->events->listEvents($calendarId); // we can pass here optionl parameters also 
            $allevents = $results->getItems();
            $allevents = $results->getItems();
            // echo "<pre>";
            // print_r($allevents) ;
            // exit();

            $data=[];
            foreach ($allevents as $event) {
               $subArr = [
                 'id'=> $event->id,
                 'title'=>$event->getSummary(),
                 'start'=>$event->getStart()->getDateTime(),
                 'end'=>$event->getEnd()->getDateTime()
               ];
               array_push($data,$subArr);
            }
             
              return  $data;
        } else {
            return redirect()->route('oauthCallback');
        }

    }
    // end function 
    // function for chrck oauth 
    public function oauth()
    {
        session_start();

        $rurl = action('gCalendarController@oauth');
        $this->client->setRedirectUri($rurl);
        if (!isset($_GET['code'])) {
            $auth_url = $this->client->createAuthUrl();
            $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);
            return redirect($filtered_url);
        } else {
            $this->client->authenticate($_GET['code']);
            $_SESSION['access_token'] = $this->client->getAccessToken();
            return redirect()->route('cal.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('calendar.createEvent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    // function for insert event into database and as well in google calender with email sending 
    public function store(createevent $request)
    {
        session_start();
        $startDateTime = $request->start_date;
        $endDateTime = $request->end_date;
        $url = URL::to("/register");
       
        $attendee_arr = $request->attendee_arr;
    
        // $attendees_Array = explode(",", $attendee_arr);
        // return $attendees_Array;
        // exit;
        $email1 = $attendee_arr[0];
        $email2 = $attendee_arr[1];
        $email3 = $attendee_arr[2];
        $email4 = $attendee_arr[3];
        $email5 = $attendee_arr[4];
        
        // block of code for inserting events in database and sending mail to attendees
        $add = new events();
        $add->title = $request->title;
        $add->start = $request->start_date;
        $add->end   = $request->end_date;
        $add->description = $request->description;
        $add->attendee1 = $email1;
        $add->attendee2 = $email2;
        $add->attendee3 = $email3;
        $add->attendee4 = $email4;
        $add->attendee5 = $email5;
        
        $add->save();
        $last_insert_id =  $add->id;
        
        $email= "jitendrakumar10898@gmail.com";
        $data = array(
               'title'=> $request->title,
               'email'=> $email,
               'sdate' => $request->start_date,
               'edate' => $request->end_date,
               'description' => $request->description,
               'signupUrl' => $url
            );
        
        Mail::to($email1)->send(new SendMail($data));
        Mail::to($email2)->send(new SendMail($data));
        Mail::to($email3)->send(new SendMail($data));
        Mail::to($email4)->send(new SendMail($data));
        Mail::to($email5)->send(new SendMail($data));
        // end of block of code 
       
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $calendarId = 'primary';
            $event = new Google_Service_Calendar_Event(
            array(
                    'summary' => $request->title,
                    'description' => $request->description,
                    'start' => ['dateTime' => $startDateTime],
                    'end' => ['dateTime' => $endDateTime],
                    'reminders' => ['useDefault' => true],
                    'attendees' => array(
                        array('email' => $email1),
                        array('email' => $email2),  
                        array('email' => $email3),
                        array('email' => $email4),
                        array('email' => $email5),
                    ),
                )   
            );
            $results = $service->events->insert($calendarId, $event);
            $google_last_insert_id =  $results->id;
            $update_sql = events::where('id',$last_insert_id)
                                 ->update(['google_event_id'=> $google_last_insert_id]);                                
            if (!$results) {
                return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            }
            // return response()->json(['status' => 'success', 'message' => 'Event Created']);

            // return redirect('/allevent');
            return "success";
        } else {
            return redirect()->route('oauthCallback');
        }
    }
   // function end 
    /**
     * Display the specified resource.
     *
     * @param $eventId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($eventId)
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $event = $service->events->get('primary', $eventId);
            if (!$event) {
                return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            }
            return response()->json(['status' => 'success', 'data' => $event]);

        } else {
            return redirect()->route('oauthCallback');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $eventId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    // function for update event from database as well from google calender also 
    public function update(editevent $request)
    {
        // return $request->title; 

        // $add->name = $request->name;
      
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);
            $startDateTime = Carbon::parse($request->input('start_date') )->toRfc3339String();
            $eventDuration = 30; //minutes

            if ($request->has('end_date')) {
                $endDateTime = Carbon::parse($request->input('end_date') )->toRfc3339String();

            } else {
                $endDateTime = Carbon::parse($request->input('start_date') )->addMinutes($eventDuration)->toRfc3339String();
            }
            // retrieve the event from the API.
            $event = $service->events->get('primary', $request->input('eventId'));
            $event->setSummary($request->title);
            $event->setDescription($request->description);
            //start time
            $start = new Google_Service_Calendar_EventDateTime();
            $start->setDateTime($startDateTime);
            $event->setStart($start);

            //end time
            $end = new Google_Service_Calendar_EventDateTime();
            $end->setDateTime($endDateTime);
            $event->setEnd($end);

            $updatedEvent = $service->events->update('primary', $event->getId(), $event);
            $eventId = $request->input('eventId');
            $title = $request->title; 
            $description = $request->description; 
            $start = $request->input('start_date'); 
            $end = $request->input('end_date');
          
            $update_query = events::where('google_event_id',$eventId)
                                  ->update(['title'=>$title,'start'=>$start, 'end'=>$end,'description'=>$description ]);

            if (!$updatedEvent) {
                return response()->json(['status' => 'error', 'message' => 'Something went wrong']);
            }
            // return response()->json(['status' => 'success', 'data' => $updatedEvent]);
            $request->session()->flash('uopdatesuccess', 'Updated Successfully');
            // return redirect('/allevent');
            return "success";

        } else {
            return redirect()->route('oauthCallback');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $eventId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    // function for delete event from database and as well google calender also 
    public function destroy($eventId)
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $service->events->delete('primary', $eventId);

            $query = events::where('google_event_id','=',$eventId)->delete();


            Session::flash('deletion_success', 'Event Deleted Successfully!'); 
            // return redirect('/allevent');
            return "success";

        } else {
            return redirect()->route('oauthCallback');
        }
    }
    // end function 
}
