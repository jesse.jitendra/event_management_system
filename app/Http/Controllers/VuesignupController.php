<?php

namespace App\Http\Controllers;

use App\vuesignup;
use Illuminate\Http\Request;
use App\Http\Requests\vuerequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;


class VuesignupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(vuerequest $request)
    {
        //
       $add = new vuesignup();
       $add->name = $request->name;
       $add->email = $request->email;
       $add->password = Hash::make($request->cpassword);
       $add->save();

       if($add){
           $request->session()->flash('signupsuccess',"Signup success");

       }else{

       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vuesignup  $vuesignup
     * @return \Illuminate\Http\Response
     */
    public function show(vuesignup $vuesignup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vuesignup  $vuesignup
     * @return \Illuminate\Http\Response
     */
    public function edit(vuesignup $vuesignup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vuesignup  $vuesignup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vuesignup $vuesignup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vuesignup  $vuesignup
     * @return \Illuminate\Http\Response
     */
    public function destroy(vuesignup $vuesignup)
    {
        //
    }
}
