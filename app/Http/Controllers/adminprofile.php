<?php

namespace App\Http\Controllers;
use App\signup;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\adminprofileedit;



class adminprofile extends Controller
{
    //
    public function index(Request $request)
    {

        
        $admin_id =  Auth::user()->id;

        $query = signup::where('id',$admin_id)
        ->update(['email'=>$request->email,'name'=>$request->name]);
        return "success";
        // return back()->with('success', 'Profile updated successfully'); 


        // $image_name = $request->file('img');
        // if($image_name == '') 
        // {
                     
        // }else{
        //     $existdata = signup::where('id',$admin_id)->first();
        //     $previous_image =  $existdata->image;
        //     $filename = public_path().'/images/'.$previous_image;
        //     // File::delete($filename);
        //     unlink($filename);
        //     // exit;
        //     // print_r($previous_image);
        //     // exit;         
        //     $imagename = time().".".$request->file('img')->extension();
        //     request()->file('img')->move(public_path('images'), $imagename);

        //     $query = signup::where('id',$admin_id)
        //                ->update(['image'=> $imagename,'email'=>$request->email]);
        //             //    return back()->with('success', 'Profile updated successfully');  
        //             return "success";         
        // }
    
    }
    public function editprofile(adminprofileedit $request)
    {
        
        $user_id  =  Auth::user()->id;
        $user_email = $request->email;
        $user_name =  $request->name;
        $image = $request->pic;
        
        if($image == '')
        {
           $query = signup::where('id',$user_id)
                    ->update(['name'=>$user_name,'email'=>$user_email]);
                    return "success";
        }else{
            $imagename = time().".".$request->file('pic')->extension();
            request()->file('pic')->move(public_path('images'), $imagename);

            $query = signup::where('id',$user_id)
            ->update(['name'=>$user_name,'email'=>$user_email,'image'=>$imagename]);
            return "success";
        }
        

    }
    public function admindetails(Request $request)
    {
        $id = Auth::user()->id;
        $query = signup::where('id',$id) 
                ->get()
                ->first();
                // echo "<pre>";
                 $email = $query->email; 
                 $name = $query->name;
                 $image = $query->image;
                $data = ['email'=>$email,'name'=>$name,'image'=>$image];
                $jsondata = json_encode($data);
                return $data;    
    }
}
