<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\signup;
use App\events;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Facade;
use Auth;
use App\Http\Requests\userprofileedit;

class userprofile extends Controller
{
    //
    public function index(Request $request)
    {
        
        $user_id  =  Auth::user()->id;
        $user_email = Auth::user()->email;
      
        $uploadFile = $request->pic;
        $image_name =  $uploadFile->store('images');
        $imagename = time().".".$request->file('img')->extension();
        request()->file('img')->move(public_path('images'), $imagename);
        
        $query = signup::where('id',$user_id)
                        ->update([ 'image'=> $image_name]);
        return response(['fileextension'=>$uploadFile->extension(),'file'=>$image_name]);

    }
    public function editprofile(userprofileedit $request)
    {
        $user_id  =  Auth::user()->id;
        $user_email = $request->email;
        $user_name =  $request->name;
        $image = $request->pic;
        
        if($image == '')
        {
           $query = signup::where('id',$user_id)
                    ->update(['name'=>$user_name,'email'=>$user_email]);
                    return "success";
        }else{
            $imagename = time().".".$request->file('pic')->extension();
            request()->file('pic')->move(public_path('images'), $imagename);

            $query = signup::where('id',$user_id)
            ->update(['name'=>$user_name,'email'=>$user_email,'image'=>$imagename]);
            return "success";
        }
       
    }
}
