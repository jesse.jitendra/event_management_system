<?php

namespace App\Http\Controllers;

use App\signup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Facade;
use Auth;

class SignupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return "your data accepting here";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    // function for signup 
    public function store(Request $request)
    {
 
        $request->validate([
         'username'=>'required',
         'email'=>'required|email|unique:users',
         'npassword'=>
         ['required','min:6'],
         'cpassword'=>'required|same:npassword'
        ]);

       $add = new signup();
       $add->name = $request->username;
       $add->email = $request->email;
       $add->password = Hash::make($request->cpassword);
       $add->save();

       $request->session()->flash('signup_success', "Signup Success Login for Continue");
       return  redirect('/login');

    }
    // end function 

    /**
     * Display the specified resource.
     *
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function show(signup $signup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function edit(signup $signup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, signup $signup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function destroy(signup $signup)
    {
        //
    }
}
