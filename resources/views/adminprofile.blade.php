@extends('layouts.header')
@section('content')
<style>
   span{
      color:red;
   }
</style>
<div class="container">
     <div class="row">
         <div class="col"></div>
         <div class="col">
         @if(Session::get('success'))
         <div class="alert alert-success">{{ Session::get('success') }}  
         <button class="btn btn-default" data-dismiss="alert">x</button>
         </div>
         @endif
         <?php
                  $email = Auth::user()->email;
                  $image = Auth::user()->image;
                  ?>
            <form   action="{{ route('editprofile') }}"   method="post"  enctype="multipart/form-data">
             <div class="form-group">
                <label for="">Email</label>
                @csrf
                <input type="email" name="email" id="" class="form-control" value="{{ $email }} ">
               <span>{{ $errors->first('email') }} </span>
             </div>
             <div class="form-grouop">
               <img src="{{ asset('images/'.$image )}}" alt="" style="height:150px; width:150px;">
             </div>
             <div class="form-group">
                 <label for="">Image</label>
                 <input type="file" name="img" id="" class="form-control"> 
                 <span>{{ $errors->first('img')   }} </span>
             </div>
             <button class="btn btn-primary" type="submit">submit</button>
            </form>
         </div>
         <div class="col"></div>
     </div>
</div>
@endsection
