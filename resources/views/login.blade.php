@extends('layouts.header')

@section('content')
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <style type="text/css">
 	span {
        color: red!important;
    }
 </style> 
 <div class="row">
	<div class="col">
		@if(Session::get('signup_success') != '')
		<div class="alert alert-success">
			<strong>Singn Up success login for continue  </strong>
			<button class="btn btn-default" data-dismiss="alert">x</button>
		</div>
		@endif
	</div>
	<div class="col border">
	<br>
	<center>Login</center><br>
		<form method="post" action="/log">
			@csrf
			<div class="form-group">
				<label>Enter Email </label>
				<input type="email" name="email" class="form-control" value="{{ old('email') }}">
				<span>{{ $errors->first('email') }}  </span>
			</div>
			<div class="form-group">
				<label>Enter Password</label>
				<input type="Password" name="password" class="form-control" value="{{ old('npassword') }}">
				<span>{{ $errors->first('npassword') }}  </span>
			</div>
			<input type="checkbox" name="remember"><label>Remember me</label>
			<br>
			<button class="btn btn-primary" type="submit">submit</button>
		</form>
		<br>
		@if(session::get('login_unsuccess') != '')
		<div class="alert alert-danger">
			<strong>Invalid credentials </strong>
			<button class="btn btn-default" data-dismiss="alert">x</button>
		</div>
		@endif
	</div>
	<div class="col"></div>
</div>
<script type="text/javascript" src="{{ asset('js/app.js')  }}"></script>
@endsection