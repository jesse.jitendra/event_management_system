@extends('layouts.header')

@section('content')
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <style type="text/css">
 	span {
        color: red!important;
    }
 </style> 
 <div class="row">
	<div class="col"></div>
	<div class="col border">
	<br>
	<center>Signup</center><br>
	
		<form method="post" action="/signup">
			@csrf
			<div class="form-group">
				<label>Enter Username </label>
				<input type="text" name="username" class="form-control" value="{{ old('username') }}">
				<span style="color:red">{{ $errors->first('username') }}  </span>
			</div>
			<div class="form-group">
				<label>Enter Email </label>
				<input type="email" name="email" class="form-control" value="{{ old('email') }}">
				<span style="color:red">{{ $errors->first('email') }}  </span>
			</div>
			<div class="form-group">
				<label>Enter Password</label>
				<input type="Password" name="npassword" class="form-control" value="{{ old('npassword') }}">
				<span style="color:red">{{ $errors->first('npassword') }}  </span>
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="Password" name="cpassword" class="form-control" value="{{ old('cpassword') }}">
				<span style="color:red">{{ $errors->first('cpassword') }}  </span>
			</div><br>
			<button class="btn btn-primary" type="submit">submit</button>
		</form>
		 
		<br>
	</div>
	<div class="col"></div>
</div>
<script type="text/javascript" src="{{ asset('js/app.js')  }}"></script>
@endsection