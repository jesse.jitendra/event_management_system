<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Event Management System</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                /* height: 100vh; */
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                /*margin-bottom: 30px;*/
            }
         
        </style>

<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->


<!--  links for use in manager dashboard and user dash board --->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/custom.css')  }} ">  
<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>

<!--  links for use in manager dashboard and user dash board theme css link --->
  <link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }} ">
  <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css')  }}">
  <link rel="stylesheet" href="{{ asset('css/style.css') }} ">
<!-- link of theme css --->
  <script src="{{ asset('vendors/js/vendor.bundle.base.js') }} "></script>
  <script src="{{  asset('vendors/js/vendor.bundle.addons.js') }}"></script>
  <script src="{{ asset('js/off-canvas.js') }}"></script>
 

<script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

 <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
  <body>
<div id="app">
<nav class="navbar navbar-expand-sm bg-light border">

  <!-- Links -->
  <ul class="navbar-nav">
  
  <li class="nav-item">
      <a class="nav-link" href="/">Home</a>
    </li>
  @if(Auth::check())
  <?php $id = Auth::user()->id;
       ?>
 
  @if($id == '1' )
  <li class="nav-item">
      <a class="nav-link" href="/allevent">Admin Dashboard</a>
    </li>
    <!-- <li class="nav-item">
      <a class="nav-link" href="/adminprofile">profile</a>
    </li> -->
    <li class="nav-item">
      <a class="nav-link" href="/logout">Logout</a>
    </li>
  @else
  <li class="nav-item">
      <a class="nav-link" href="/event">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/logout">Logout</a>
    </li>
  @endif  
    
      
    @else
    <!-- <li class="nav-item">
      <a class="nav-link" href="/login">Login </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/register">Signup</a>
    </li> -->
    <li class="nav-item">
      <a class="nav-link"> <router-link to="/login">Login</router-link>  </a>
    </li>
    <li class="nav-item">
      <a class="nav-link"> <router-link to="/signup">Signup</router-link>  </a>
    </li>
    <!-- <li class="nav-item">
      <a class="nav-link" href="/login">Manager Login</a>
    </li> -->
    @endif
  </ul>

</nav>
<div>
  @yield('content')
</div>
<div>
@yield('contenttwo')
</div>
<div class="container">
  @yield('calendercontent')
</div>

</body>
</html>