@extends('layouts.header')

@section('contenttwo')
<script src="{{ asset('js/misc.js') }}"></script>
<div class="container-fluid">
    <div class="row">
         @section('content')
            <managerdashboard-component></managerdashboard-component>  
        </div>
        <script type="text/javascript" src="{{ asset('js/app.js')  }}"></script>
        @endsection
    </div>
</div>

 <!-- Custom js for this page-->
 <script src="{{ asset('js/dashboard.js') }}"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>
@endsection
