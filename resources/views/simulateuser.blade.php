@extends('layouts.header')

@section('content')
<!DOCTYPE html>
<html>
<head>
  <title></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" rel="stylesheet" />
</head>
<body>
   <div class="container">
      <div class="row">
          <div class="col-3">
            for see these events on your google calender <a href="https://accounts.google.com/signin/v2/identifier?service=cl&passive=1209600&osid=1&continue=https%3A%2F%2Fcalendar.google.com%2Fcalendar%2Frender&followup=https%3A%2F%2Fcalendar.google.com%2Fcalendar%2Frender&scc=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin" target="blank">Click here..</a>
          </div>
          <div class="col-9">
              <?php //echo  Auth::user()->id;  ?>
                User :-  {{ $name }}
              <div id="calendar"> </div>    
          </div>
      </div>
   </div>
   <?php 
      $jsondata = json_encode($data); 
    ?>
   <script type="text/javascript">
  $(document).ready(function() {
  // page is now ready, initialize the calendar...
  $('#calendar').fullCalendar({
   
   events : {!! $jsondata !!}
   
  });
  });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>

@endsection



