@extends('layouts.header')
 
@section('content')
	<router-view></router-view>
</div>

<script type="text/javascript" src="{{ asset('js/app.js')  }}"></script>
@endsection