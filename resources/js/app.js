/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const signup = require('./components/signupthemeComponent.vue').default;
const home  = require('./components/homepagecomponent.vue').default;
const login = require('./components/loginComponent.vue').default;
const adminCalender = require('./components/admincalenderComponent.vue').default;
const allusers = require('./components/allusersComponent.vue').default;
const adminprofile = require('./components/adminprofileComponent.vue').default;
const userdash  = require('./components/usercalenderComponent.vue').default;
const eventlist = require('./components/eventlistComponent.vue').default;
const createevent = require('./components/createeventComponent.vue').default;
const usercalender = require('./components/userdashcalenderComponent.vue').default;
const userprofile  = require('./components/userprofileComponent.vue').default;

const routes = [
          {
          	path : '/signup',
          	component : signup
          },
          {
              path : '/userprofile',
              component : userprofile
          },
          {
              path : '/',
              component : home
          },
          {
              path : '/createevent',
              component:createevent

          },
          {
              path : '/',
              component : adminCalender
          },
          {
              path: '/admincalender',
              component :adminCalender
          },
          {
              path : '/usercalender',
              component : usercalender
          },
          {
              path : '/login',
              component : login
          },
          {
              path : '/allusers',
              component : allusers
          },
          {
              path : '/adminprofile',
              component : adminprofile
          },
          {
              path : '/userdash/:id',
              component : userdash
          },
          {
              path : '/eventlist',
              component : eventlist
          }

];

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('homepage-component', require('./components/homepagecomponent.vue').default);
Vue.component('managerdashboard-component', require('./components/managerdashboardComponent.vue').default);
Vue.component('userdashboard-component', require('./components/userdashboardComponent.vue').default);

const router = new VueRouter({
	 routes
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.prototype.$eventBus = new Vue(); // Global event bus


const app = new Vue({
    el: '#app',
    router,
});


